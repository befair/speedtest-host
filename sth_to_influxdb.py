#!/usr/bin/env python3

import os
import requests
from csv import DictReader
from io import StringIO
import reactivex as rx
from reactivex import operators as ops
from influxdb_client import InfluxDBClient, Point, WriteOptions
from influxdb_client.client.write_api import SYNCHRONOUS
from dotenv import load_dotenv


load_dotenv()  # take environment variables from .env.

INFLUXDB_URL = os.environ.get("INFLUXDB_URL", "http://influxdb:8086")
INFLUXDB_BUCKET = "sth"
INFLUXDB_ORG = os.environ["INFLUXDB_ORG"]
INFLUXDB_TOKEN = os.environ["INFLUXDB_TOKEN"]
INFLUXDB_MEASUREMENT = "speedtest"

# STH_CSV_URL i.e: http://<ip>:10000/sth/csv/flt_rows=no_errors
STH_CSV_URL = os.environ["STH_CSV_URL"]


def parse_row(row):
    """Parse row of CSV file into Point. Headers:
    
    Server ID,Sponsor,Server Name,Timestamp,Distance,Ping,Download,Upload,Share,IP Address
    """
    # print(f"{row['Timestamp']=}")

    try:
        return Point(INFLUXDB_MEASUREMENT) \
            .tag("site", "DIREZIONALE") \
            .tag("server_id", row['Server ID']) \
            .tag("sponsor", row['Sponsor']) \
            .tag("server_name", row['Server Name']) \
            .field("distance", float(row['Distance'])) \
            .field("ping", float(row['Ping'])) \
            .field("download", float(row['Download'])) \
            .field("upload", float(row['Upload'])) \
            .tag("share", row['Share']) \
            .tag("ip_address", row['IP Address']) \
            .time(row['Timestamp'])

    except Exception as e:
        print(e)
        return None


def main():

    csv_url = STH_CSV_URL
    csv_data = requests.get(csv_url).content
    # print(csv_data)
    headers = "Cli Timestamp,Server ID,Sponsor,Server Name,Timestamp,Distance,Ping,Download,Upload,Share,IP Address".split(",")

    # Converts csv data into sequence of data point
    data = rx \
        .from_iterable(DictReader(StringIO(csv_data.decode('utf-8')), fieldnames=headers)) \
        .pipe(ops.map(lambda row: parse_row(row)), ops.filter(lambda x: x is not None))

    with InfluxDBClient(url=INFLUXDB_URL, token=INFLUXDB_TOKEN, org=INFLUXDB_ORG) as client:

        with client.write_api(write_options=WriteOptions(batch_size=1000, flush_interval=10000)) as write_api:

            write_api.write(bucket=INFLUXDB_BUCKET, record=data)


if __name__ == "__main__":
    main()


