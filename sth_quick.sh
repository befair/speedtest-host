#!/bin/sh

FNAME=${1:-/var/log/speedtest.csv}

while true; do 
    echo "$(date -Iseconds --utc),$(speedtest-cli --csv)"
    sleep 1800;
done >> $FNAME
