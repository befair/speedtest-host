#!/usr/bin/env python

import os
from bottle import route, run, template, response, request

STH_CSV=os.environ.get("STH_CSV", "/var/log/speedtest.csv")
STH_HUMAN=STH_CSV.replace(".csv",".txt")

@route('/sth/<kind>/')
def index(kind):
    if kind == "csv":
        fname = STH_CSV
        response.set_header('Content-Type', 'text/plain')
    elif kind == "human":
        fname = STH_HUMAN
        response.set_header('Content-Type', 'text/plain')

    with open(fname, "r") as f:
        rows = f.readlines()

    if request.query.get("flt_rows") == "no_errors":
        new_rows = []
        for row in rows:
            if "error" in row.lower() or "cannot" in row.lower():
                pass
            else:
                new_rows.append(row)
        rows = new_rows.copy()

    return "".join(rows)

run(host='0.0.0.0', port=10000)
