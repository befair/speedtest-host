# SpeedTest Host

To install in Alpine Linux you can use just `# ./setup_alpine.sh`

Or:

Install speedtest-cli:

    `# apk add speedtest-cli`
    `# apt install speedtest-cli`

Install [bottle.py](https://bottlepy.org) with:
`wget https://github.com/bottlepy/bottle/raw/master/bottle.py`


Add crontabs entry:

```
*/30 *  *  *  *  echo "$(date -Iseconds --utc),$(speedtest-cli --csv)" >> /var/log/speedtest.csv;
*/15 *  *  *  *  /opt/speedtest-host/sth_update_csv > /var/log/speedtest.txt;
```

WARNING: needs bash, not ash.

Execute `./run_sth.py`


Point your browser to:

    http://<ip address>:10000/sth/csv/
    http://<ip address>:10000/sth/human/

You can add the querystring parameter: `?flt_rows=no_errors` to skip error lines... i.e.:

    http://localhost:10000/sth/csv/?flt_rows=no_errors
    http://<ip address>:10000/sth/human/?flt_rows=no_errors
